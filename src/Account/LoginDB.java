package Account;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Base64;
import java.util.Scanner;

public class LoginDB {
    public static int signUp(Connection connection, Scanner scanner) {
        int res = 0;
        String login = chooseLogin(connection, scanner);
        if(login.equals(""))
            return 0;

        String password = "";
        while(password.equals("")) {
            System.out.println("Enter a password or press 0 to break");
            password = scanner.nextLine();
            if(password.equals("0"))
                return 0;
        }

        String encodedPassword = Base64.getEncoder().encodeToString(password.getBytes());
        res = insertIntoTable(connection, login, encodedPassword);
        return res;
    }

    private static int insertIntoTable(Connection connection, String login, String password) {
        int res = 0;
        try {
            String query = "INSERT into logins(login, password) VALUES ('"+login+"', '" + password + "') returning id;";
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);
            rs.next();
            res = rs.getInt(1);
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return res;
    }

    private static String chooseLogin(Connection connection, Scanner scanner) {
        String msg = "Enter a login or press 0 to return";
        String login = "";
        scanner.nextLine();
        int res = 1;

        while(res != 0) {
            System.out.println(msg);
            login = scanner.nextLine();
            if(login.equals("0"))
                return "";

            res = checkLogin(connection, login);
            msg = "Login exists, choose a new one";
        }

        return login;
    }

    private static int checkLogin(Connection connection, String login) {
        int res = 1;
        ResultSet rs;

        try {
            String query = "SELECT count(*) from logins where login = '" + login + "';";
            Statement statement = connection.createStatement();
            rs = statement.executeQuery(query);
            rs.next();
            res = rs.getInt(1);
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return res;
    }

    public static void changePassword(Connection connection, Scanner scanner, int loginId) {
        scanner.nextLine();
        System.out.println("Enter a new password or press 0 to return");
        String password = scanner.nextLine();
        if(password.equals("0"))
            return;

        String encodedPassword = Base64.getEncoder().encodeToString(password.getBytes());
        updatePassword(connection, loginId, encodedPassword);
    }

    private static void updatePassword(Connection connection, int loginId, String password) {
        try {
            String query = "UPDATE logins set password = '" + password + "' where id = " + loginId + ";";
            Statement statement = connection.createStatement();
            statement.executeUpdate(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void deleteAccount(Connection connection, int login) {
        try {
            String query = "delete from logins where id = " + login + ";";
            Statement statement = connection.createStatement();
            statement.executeUpdate(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
