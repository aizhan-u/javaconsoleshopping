package Cart;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;

public class ItemsDB {
    public static void fillItemsTable(Connection connection) {
        try {
            String query = "insert into items(name, price, count)\n" +
                    "values\n" +
                    "('Гель SANFOR Белизна', 754, 50),\n" +
                    "('Влажные салфетки AURA антибактериальные, 72шт', 871, 50),\n" +
                    "('Напиток Coca-Cola 1 л ПЭТ', 297, 40),\n" +
                    "('БАТОНЧИК MARS SNICKERS SUPER 95ГР СТИК', 250, 32),\n" +
                    "('Йогурт питьевой Epica 2,5% 290 гр', 461, 46)" +
                    "ON CONFLICT DO NOTHING;";
            Statement statement = connection.createStatement();
            statement.executeUpdate(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showAllItems(Connection connection) {
        try {
            String query = "SELECT id, name, price from items where count > 0;";
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);
            ResultSetMetaData rsmd = rs.getMetaData();
            int count = rsmd.getColumnCount();

            for(int i = 1; i <= count; i ++) {
                System.out.print(rsmd.getColumnName(i) + "\t");
            }
            System.out.println("");

            while(rs.next()) {
                for(int i = 1; i <= count; i ++) {
                    System.out.print(rs.getString(i) + "\t");
                }
                System.out.println("");
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }
}
