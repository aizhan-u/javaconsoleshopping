package Cart;

import Account.UserDB;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.Scanner;

public class CartDB {
    public static void cartMain(Connection connection, Scanner scanner, int login) {
        int money;
        System.out.println("What do you want to do?\n0\tReturn\n1\tAdd item to cart\n2\tRemove/reduce item\n3\tEmpty cart\n4\tBuy everything from cart\n5\tShow cart\n6\tAdd money\n7\tShow money\n8\tWithdraw money");
        int command = scanner.nextInt();
        while(command != 0) {
            if(command == 1) {
                addItems(connection, scanner, login);
            } else if(command == 2) {
                showUserCart(connection, login);
                reduceItems(connection, scanner, login);
            } else if(command == 3) {
                emptyCart(connection, login);
            } else if(command == 4) {
                int sum = cartSum(connection, login);
                System.out.println("Total amount of the cart is: " + sum + " tenge.");
                money = UserDB.showMoney(connection, login);
                if(money < sum) {
                    System.out.println("You have " + UserDB.showMoney(connection, login) + " tenges. Please, review your cart or add more money.");
                } else {
                    UserDB.addMoney(connection, login, -sum);
                    System.out.println("Success!");
                    emptyCartAfterBuy(connection, login, changeItemsCount(connection, login));
                }
            } else if(command == 5) {
                showUserCart(connection, login);
            } else if(command == 6) {
                System.out.println("Enter the amount:");
                UserDB.addMoney(connection, login, scanner.nextInt());
            } else if(command == 7) {
                System.out.println("You have " + UserDB.showMoney(connection, login) + " tenges left");
            } else if(command == 8) {
                System.out.println("Enter the amount:");
                UserDB.addMoney(connection, login, -scanner.nextInt());
            }
            System.out.println("What do you want to do?\n0\tReturn\n1\tAdd item to cart\n2\tRemove/reduce item\n3\tEmpty cart\n4\tBuy everything from cart\n5\tShow cart\n6\tAdd money\n7\tShow money\n8\tWithdraw money");
            command = scanner.nextInt();
        }
    }

    private static String changeItemsCount(Connection connection, int login) {
        String vals = "";
        try {
            String query = "select item_id, least(user_cart.count, items.count) from user_cart left join items on user_cart.item_id = items.id\n" +
                    "where login_id = " + login + "\n" +
                    "and items.count > 0;";
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);

            while(rs.next()) {
                vals = vals + "(" + rs.getInt(1) + ", " + rs.getInt(2) + "),\n";
            }

            if(!vals.equals("")) {
                query = "UPDATE items set\ncount = count - c.amn\nfrom (values\n" + vals.substring(0, vals.length() - 2) + "\n) as c(item_id, amn)\nwhere c.item_id = items.id;";
                statement.executeUpdate(query);
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return vals;
    }

    private static int cartSum(Connection connection, int login) {
        int sum = 0;
        try {
            String query = "select sum(Total) from (\n" +
                    "select least(user_cart.count, items.count) * items.price as Total\n" +
                    "from user_cart left join items on user_cart.item_id = items.id\n" +
                    "where login_id = " + login  + "\n" +
                    "and items.count > 0\n" +
                    ") as cart;";
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);
            if(rs.next()) {
                sum = rs.getInt(1);
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return sum;
    }

    private static void emptyCartAfterBuy(Connection connection, int login, String vals) {
        try {
            String query;
            Statement statement = connection.createStatement();

            if(!vals.equals("")) {
                query = "UPDATE user_cart set\ncount = count - c.amn\nfrom (values\n" + vals.substring(0, vals.length() - 2) + "\n) as c(item_id, amn)\nwhere c.item_id = user_cart.item_id;";
                statement.executeUpdate(query);
            }

            query = "delete from user_cart where login_id = " + login + " and count = 0;";
            statement.executeUpdate(query);
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    private static void emptyCart(Connection connection, int login) {
        try {
            String query = "delete from user_cart where login_id = " + login + ";";
            Statement statement = connection.createStatement();
            statement.executeUpdate(query);
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    private static void reduceItems(Connection connection, Scanner scanner, int login) {
        System.out.println("Select an item id you want to delete/reduce or press 0 to return");
        int item = scanner.nextInt();
        if(item == 0) {
            return;
        }
        int count = getCartItemCount(connection, login, item);
        System.out.println("How many of " + count + " items you want to remove?");
        int rmv = scanner.nextInt();
        if(rmv == 0) {
            return;
        } else if(count > rmv) {
            updateUserCartItem(connection, login, item, -rmv);
        } else {
            deleteUserCartItem(connection, login, item);
        }
    }

    private static void deleteUserCartItem(Connection connection, int login, int item) {
        try {
            String query = "delete from user_cart where login_id = " + login + " and item_id = " + item + ";";
            Statement statement = connection.createStatement();
            statement.executeUpdate(query);
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    private static int getCartItemCount(Connection connection, int login, int item) {
        int res = 0;
        try {
            String query = "SELECT count from user_cart where login_id = " + login + " and item_id = " + item + ";";
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);
            if(rs.next()) {
                res = rs.getInt(1);
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return res;
    }

    private static void addItems(Connection connection, Scanner scanner, int login) {
        System.out.println("Choose the id of an item you want to buy or press 0 to return");
        ItemsDB.showAllItems(connection);
        int item = scanner.nextInt();
        if(item == 0) {
            return;
        }
        System.out.println("How many items you want to buy?");
        int count = scanner.nextInt();
        if(count == 0) {
            return;
        }
        if(checkCartItemExists(connection, login, item)) {
            updateUserCartItem(connection, login, item, count);
        } else {
            insertUserCartItem(connection, login, item, count);
        }
    }

    private static boolean checkCartItemExists(Connection connection, int login, int item) {
        try {
            String query = "SELECT * from user_cart where login_id = " + login + " and item_id = " + item + ";";
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);

            if(rs.next()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return false;
    }

    private static void insertUserCartItem(Connection connection, int login, int item, int count) {
        try {
            String query = "INSERT into user_cart(login_id, item_id, count)\n"+
                            "VALUES(" + login + ", " + item + ", " + count + ");";
            Statement statement = connection.createStatement();
            int rs = statement.executeUpdate(query);
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    private static void updateUserCartItem(Connection connection, int login, int item, int count) {
        try {
            String query = "update user_cart\n" +
                    "set count = count +" + count + "\n" +
                    "where login_id = " + login + " and item_id = " + item + ";";
            Statement statement = connection.createStatement();
            int rs = statement.executeUpdate(query);
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    private static void showUserCart(Connection connection, int login) {
        System.out.println("Your cart:");
        try {
            String query = "select items.id as ID, name as Name, items.price as Price, user_cart.count as Count, user_cart.count * items.price as Total\n" +
                    "from user_cart left join items on user_cart.item_id = items.id\n" +
                    "where login_id = " + login + ";";
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);
            ResultSetMetaData rsmd = rs.getMetaData();
            int count = rsmd.getColumnCount();

            for(int i = 1; i <= count; i ++) {
                System.out.print(rsmd.getColumnName(i) + "\t");
            }
            System.out.println("");

            while(rs.next()) {
                for(int i = 1; i <= count; i ++) {
                    System.out.print(rs.getString(i) + "\t");
                }
                System.out.println("");
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }
}
